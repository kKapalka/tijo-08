package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PawnTest {

    private RulesOfGame pawn;
    private Point2d pointFrom;
    private Point2d pointTo;

    @Before
    public void initDataForPawn() {

        pawn = new Pawn();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForPawn() {

        // TODO: Prosze dokonczyc implementacje testow...

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                pawn.isCorrectMove(pointFrom, pointTo));
    }
}
