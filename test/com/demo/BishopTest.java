package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BishopTest {

    private RulesOfGame bishop;
    private Point2d pointFrom;
    private Point2d pointTo;

    @Before
    public void initDataForRook() {

        bishop = new Bishop();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForBishop() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(20, 20);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(-21, -21);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, 4);
        pointTo = new Point2d(-3, 2);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(2, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(1, -1);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                bishop.isCorrectMove(pointFrom, pointTo));
    }
}
