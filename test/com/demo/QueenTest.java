package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class QueenTest {

    private RulesOfGame queen;
    private Point2d pointFrom;
    private Point2d pointTo;

    @Before
    public void initDataForQueen() {

        queen = new Queen();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForQueen() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 10);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-10, -1);
        pointTo = new Point2d(10, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 1);
        pointTo = new Point2d(-1, -2);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(20, 20);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(-21, -21);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, 4);
        pointTo = new Point2d(-3, 2);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(2, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(0, 1);
        pointTo = new Point2d(1, -1);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                queen.isCorrectMove(pointFrom, pointTo));
    }
}
