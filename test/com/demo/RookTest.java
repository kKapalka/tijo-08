package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RookTest {

    private RulesOfGame rook;
    private Point2d pointFrom;
    private Point2d pointTo;

    @Before
    public void initDataForRook() {

        rook = new Rook();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForRook() {

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 10);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                rook.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-10, -1);
        pointTo = new Point2d(10, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                rook.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -1);
        pointTo = new Point2d(5, 5);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                rook.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 1);
        pointTo = new Point2d(-1, -2);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                rook.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                rook.isCorrectMove(pointFrom, pointTo));
    }
}
