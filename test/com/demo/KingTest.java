package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class KingTest {

    private RulesOfGame king;
    private Point2d pointFrom;
    private Point2d pointTo;

    @Before
    public void initDataForKing() {

        king = new King();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForKing() {

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, 0);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, 1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(1, 1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, 1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, 0);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(2, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(1, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(1, 0);
        pointTo = new Point2d(0, -1);
        Assert.assertTrue("Correct move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -2);
        pointTo = new Point2d(-1, 0);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(-1, -2);
        pointTo = new Point2d(-3, 0);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        Assert.assertFalse("Incorrect move from" + pointFrom + "to" + pointTo,
                king.isCorrectMove(pointFrom, pointTo));
    }
}
